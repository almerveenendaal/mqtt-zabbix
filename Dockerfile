# Basisimage met Node.js
FROM node:18-alpine

# Werkdirectory instellen
WORKDIR /app

# Kopieer package.json en package-lock.json en installeer dependencies
COPY package*.json ./
RUN npm install

# Kopieer de rest van de bestanden
COPY . .

# Start de server
CMD ["node", "index.js"]
