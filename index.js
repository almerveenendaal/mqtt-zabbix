
console.log('Starting mqtt-zabbix')

require('console-stamp')(console, '[dd-mm-yyyy HH:MM:ss.l]')

var ZabbixSender = require('node-zabbix-sender');
var sender = new ZabbixSender({host: "192.168.123.11"});

var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://192.168.123.11',{
    username: 'camper',
    password: 'rijdenMaar'
})

client.on('error', function (m) {
  console.log(m)
})
 
client.on('connect', function () {
  console.log('Verbonden met MQTT server')
  client.subscribe('zigbee2mqtt/#')
  //client.subscribe('zigbee2mqtt/0x00158d000549afab')
  //client.subscribe('zigbee2mqtt/0x00158d000540f78a') 
  //client.subscribe('zigbee2mqtt/waterlevel')
  //client.subscribe('zigbee2mqtt/mikrotik') 
  //client.subscribe('zigbee2mqtt/victron') 
  client.subscribe('camper/#') 
  //client.subscribe('camper/gps') 
})
 
client.on('message', function (topic, message) {
  message = check_rewrites(message.toString())

  console.log(topic.split('/')[1],topic)
  if(topic.split('/')[1] == 'bridge') return false
	
  sender.addItem('Camper', topic.replace(/\//g,'.'),message)
  sender.send(function(err, res,items) {
    if (err) console.error(err) 
    if(res.response != 'success') console.warn(res,items)

    if(res.info){
      res.info = res.info.split('; ')  
      let failed = res.info[1].split(': ')[1]

      if(failed > 0) console.log(JSON.stringify(res),items)
    }
  });

})


function check_rewrites(message){
  // checken of het bericht aan bepaalde voorwaarde voldoet
  if(message.substr(0,1) == '{'){
    message = JSON.parse(message)

    if(message.rsrp) message.rsrp_parsed = rsrp(message.rsrp)
    if(message.rsrq) message.rsrq_parsed = rsrp(message.rsrq)
    if(message.sinr) message.sinr_parsed = sinr(message.sinr)
    if(message.cqi) message.cqi_parsed = sinr(message.cqi)
    message = JSON.stringify(message)
  }

  return message
}

function rsrp(input){
  input = parseInt(input.replace('dBm',''))

  if(input < -100) return 'Very low'
  else if(input < -90) return 'Weak'
  else if(input < -80) return 'Good'
  else if(input > -80) return 'Excellent'
}

function rsrq(input){
  input = parseInt(input.replace('dB',''))

  if(input < -20) return 'Very low'
  else if(input < -15) return 'Weak'
  else if(input < -10) return 'Good'
  else if(input > -10) return 'Excellent'
}

function sinr(input){
  input = parseInt(input.replace('dB',''))

  if(input < 0) return 'Very low'
  else if(input < 13) return 'Weak'
  else if(input < 20) return 'Good'
  else if(input > 20) return 'Excellent'
}

function cqi(input){
  input = parseInt(input)

  if(input > 10) return 'Good'
  else if(input > 6) return 'Fair'
  else return 'Bad'
}
